const Discord = require("discord.js");
const client = new Discord.Client();
const request = require('request');
window.$ = window.jQuery = require('jquery');

const time = new Date();
const embed = new Discord.RichEmbed()
    .setTitle("Discord Bot")
    .setAuthor("XD0M3","https://cdn.discordapp.com/avatars/257525341067804673/9464bcfb7cc853a00a1e1a2a92b4f409.png")
    .setColor("#f44242")
    .setDescription("Hello, if you want to know how I wrote this code. You can look at the repository")
    .setThumbnail("https://i.imgur.com/8TC6vlO.png")
    .setFooter("So if you have questions just ask :P")
    .setURL("https://bitbucket.org/gruenbergerdominik/discord_bot/src/master/")
    .addField("Framework: ", "nodeJs/electron", true)
    .addField("Language: ", "Javascript", true)
    .addField("Design UI: ", "HTML/CSS", true)
    .addField("Idea: ","Vic <3", true);

let guild;
let dmc_array;
let channels;

let watching_channel_id;

client.on("ready", () => {
  guild = client.guilds.get("401746058188816384");
  channels = guild.channels;
  dmc_array = guild.channels.filter(c => c.type == "text" && c.deleted == false).map(c => ({
      id: c.id,
      name: c.name
  }));
  console.log(channels);
  console.log(dmc_array);
  dmc_array.forEach(x => {
      $('#channelList').append('<div class="row item" id="' + x.    id + '"><div class="col-12">' + x.name + '</div></div>');
      $('#' + x.id).click((l)=> {
        $('.menuActive').removeClass('menuActive');
        $('#' + x.id).addClass("menuActive");
        watching_channel_id = x.id;
        channels.get(watching_channel_id).send("Now watching here!");
      });
  });
  $('#426816205668614145').addClass("menuActive");
  watching_channel_id = "426816205668614145";
  //channels.get(watching_channel_id).send("Now watching here!");
});
 
client.on("message", (message) => {
    console.log(message);
  if(message.channel.id == watching_channel_id && message.author.id != client.user.id){
    commands(message);
  }
});

$('#send').click(()=> {
    channels.get(watching_channel_id).send($('#message').val());
    $('#message').val("");
});

$('#embed').click(() => {   
    channels.get(watching_channel_id).send({embed});
});

function commands(m){
    let content = m.content;
    let author = m.author;

    if(content.includes("!")){
        let args = content.trim().split("!")[1].split(" ");
        let command = args[0];
        args.splice(0,1);

        //DEBUG
        console.log(command);
        console.log(args);

        //COMMANDS
        if(command == "code"){
            channels.get(watching_channel_id).send({embed});
        } else if(command == "up"){
            let n = new Date() - time;
            channels.get(watching_channel_id).send("The Server is running for " + secondsToString(n/1000)+ "!");
        } else if(command == "cups"){
            let n = 5;
            if(args.length != 0) n = args[0];
            console.log(args.length);
            let cup_array;
            let result = [];
            request('https://api.eslgaming.com/play/v1/leagues?fields=name,timeline,uri&tags=go4lol_euw&path=%2Fplay%2Fleagueoflegends%2F&states=upcoming&limit.total='+n, function (error, response, body) {
                //console.log('error:', error); // Print the error if one occurred
                //console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
                //console.log('body:', body); // Print the HTML for the Google homepage.
                cup_array = JSON.parse(body);
                for(let i in cup_array){
                    result.push([i,cup_array[i]]);
                }
                //cup_array = JSON.parse(body);
                request('https://api.eslgaming.com/play/v1/leagues?fields=name,timeline,uri&tags=go4lol_eune&path=%2Fplay%2Fleagueoflegends%2F&states=upcoming&limit.total='+n, function (errorr, responsee, bodyy) {
                    //console.log('error:', error); // Print the error if one occurred
                    //console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
                    //console.log('body:', body); // Print the HTML for the Google homepage.
                    //cup_array += JSON.parse(bodyy);
                    cup_array = JSON.parse(bodyy);
                    for(let i in cup_array){
                        result.push([i,cup_array[i]]);
                    }
                    
                    let cups = new Discord.RichEmbed()
                        .setTitle("**The next cups**")
                        .setAuthor("XD0M3 Bot",client.user.avatarURL)
                        .setColor("#f44242")
                        .setDescription("Here is the list for the next cups.")
                        .setURL("https://play.eslgaming.com/leagueoflegends/");

                    for(let x in result){
                        let b = result[x];
                        //console.log(b);
                        let id = b[0]
                        let name = b[1]["name"]["full"];
                        let uri = b[1]["uri"];
                        let date = b[1]["timeline"]["inProgress"]["begin"];
                        let dates = new Date(Date.parse(date));
                        //console.log(dates);
                        var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric',timeZone: 'Europe/Vienna'};
                        cups.addField(name, "["+ dates.toLocaleDateString('en-DE', options) + " + " + dates.toLocaleTimeString('en-EN') +" (CET)](https://play.eslgaming.com/"+ uri + ")")
                    }
                    channels.get(watching_channel_id).send({embed: cups});
                });
            });
        }
    }
}

client.login("NDk2NzI4MTc5MzkwNTQ1OTQw.DpYFVQ.urrB5wqbovMYV4nwbkSQkMl42Rg");

function secondsToString(seconds){
    var numdays = Math.floor(seconds / 86400);
    var numhours = Math.floor((seconds % 86400) / 3600);
    var numminutes = Math.floor(((seconds % 86400) % 3600) / 60);
    var numseconds = Math.floor(((seconds % 86400) % 3600) % 60);
    return numdays + " days " + numhours + " hours " + numminutes + " minutes " + numseconds + " seconds";
}
